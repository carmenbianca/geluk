#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup
import logging
import os

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

VERSION = '0.0.1'
VERSION_TEXT = (
    '# This file was generated from setup.py\n'
    "version = '{version}'\n"
    "sha = '{sha}'\n"
)
VERSION_FILE = 'geluk/_version.py'
NO_LOCAL_VERSION = os.getenv('NO_LOCAL_VERSION') == 'true'

requirements = [
    # PyQt5 is explicitly not listed as requirement here.
    # TODO: put package requirements here
]

setup_requirements = [
    'pytest-runner',
]

test_requirements = [
    'pytest',
]


def parse_git_describe(tag):
    parts = tag.split('-')
    # Possibilities:
    # version
    # version-dirty
    # version-distance-commit
    # version-distance-commit-dirty
    assert 1 <= len(parts) <= 4

    version_parts = {
        # Ignore tag version. Tag version won't match on release
        # candidates.
        'version': VERSION,
        'distance': parts[1] if len(parts) >= 3 else '',
        'commit': parts[2] if len(parts) >= 3 else '',
        'dirty': 'dirty' if 'dirty' in parts else '',
    }

    if version_parts['distance']:  # and thus also version_parts['commit']
        result = '{}.dev{}+{}{}'.format(
            version_parts['version'],
            int(version_parts['distance']) - 1,
            version_parts['commit'],
            '.' + version_parts['dirty'] if version_parts['dirty'] else '',
        )
    else:
        result = '{}{}'.format(
            version_parts['version'],
            '+' + version_parts['dirty'] if version_parts['dirty'] else '',
        )

    if NO_LOCAL_VERSION:
        return result.split('+')[0]
    else:
        return result


def git_version(version):
    """Return version with local version identifier."""
    try:
        import git
    except ImportError:
        logging.error('could not find GitPython')
        return version
    try:
        repo = git.Repo('.')
    except:
        logging.error('could not find git repo')
        return version
    try:
        tag = repo.git.describe(match='v[0-9]*', tags=True, dirty=True)
    except git.exc.GitCommandError:
        logging.error('could not find version tags')
        return version

    return parse_git_describe(tag)


def git_sha():
    try:
        import git
        repo = git.Repo('.')
        return repo.head.commit.hexsha
    except:
        return ''


version = git_version(VERSION)
sha = git_sha()
with open(VERSION_FILE, 'w') as f:
    logging.info('writing version to {}'.format(VERSION_FILE))
    f.write(VERSION_TEXT.format(version=version, sha=sha))


setup(
    name='geluk',
    version=version,
    description="Geluk shows fortune cookies",
    long_description=readme + '\n\n' + history,
    author="Carmen Bianca Bakker",
    author_email='c.b.bakker@carmenbianca.eu',
    url='https://gitlab.com/carmenbianca/geluk',
    packages=[
        'geluk',
    ],
    package_dir={'geluk': 'geluk'},
    entry_points={
        'gui_scripts': [
            'geluk-qt = geluk.main:main',
        ]
    },
    include_package_data=True,
    install_requires=requirements,
    license="GNU General Public License v3",
    zip_safe=False,
    keywords='geluk',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
    ],
    test_suite='tests',
    tests_require=test_requirements,
    setup_requires=setup_requirements,
)
