#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2016  Carmen Bianca Bakker <c.b.bakker@carmenbianca.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from PyQt5 import Qt


class MainWidget(Qt.QWidget):

    def __init__(self, parent=None):
        super().__init__(parent)


class MainWindow(Qt.QMainWindow):

    def __init__(self, parent=None):
        super().__init__(parent)

        centralWidget = MainWidget(self)
        self.setCentralWidget(centralWidget)
