#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2016  Carmen Bianca Bakker <c.b.bakker@carmenbianca.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import logging

_logger = logging.getLogger(__name__)


def main():
    try:
        from PyQt5 import Qt
        from . import gui
    except ImportError:
        _logger.critical('PyQt5 is not available')
        sys.exit(1)
    app = Qt.QApplication(sys.argv)
    form = gui.MainWindow()
    form.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
