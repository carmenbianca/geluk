# -*- coding: utf-8 -*-
#
# Copyright (C) 2016  Carmen Bianca Bakker <c.b.bakker@carmenbianca.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = """Carmen Bianca Bakker"""
__email__ = 'c.b.bakker@carmenbianca.eu'

try:
    from ._version import version as __version__
    from ._version import sha as git_commit
except:   # pragma: no cover
    __version__ = '0.0.1'
    git_commit = ''
