# -*- coding: utf-8 -*-
#
# Copyright (C) 2016  Carmen Bianca Bakker <c.b.bakker@carmenbianca.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import subprocess
import pathlib
import logging

FORTUNE_EXE = 'fortune'
PATHS = [
    pathlib.PosixPath('/usr/share/fortune'),  # SUSE
    pathlib.PosixPath('/usr/share/games/fortunes'),  # Debian
]

_logger = logging.getLogger(__name__)


class Cookie:

    #: No offensive cookies.
    NOT_OFFENSIVE = 0x1
    #: Only offensive cookies.
    OFFENSIVE = 0x2
    #: All cookies, whether offensive or not.
    ALL = NOT_OFFENSIVE | OFFENSIVE

    def __init__(self, text, fortune_file=''):
        """:param str text: Contents of cookie.
        :keyword str fortune_file: File that cookie was drawn from.
        """
        self.text = text
        self.fortune_file = fortune_file

    @classmethod
    def get_cookie(cls, fortune_files=None, offence=NOT_OFFENSIVE):
        """Generate a :class:`Cookie` by invoking ``fortune`` to the system.

        :keyword fortune_files: List of fortune files to draw a cookie from.
        :type fortune_files: iterable of :class:`str`
        :keyword int offence: What offensiveness rules to apply.
        :return: A random cookie.
        :rtype: :class:`Cookie`
        """
        if fortune_files is None:
            fortune_files = []
        command = [FORTUNE_EXE, '-c'] + fortune_files
        if offence == cls.OFFENSIVE:
            command.append('-o')
        elif offence == cls.ALL:
            command.append('-a')
        _logger.info('Executing {}'.format(' '.join(command)))
        # Ideally I'd like to use `subprocess.run` here, but this is exclusive
        # to Python >=3.5.  This will suffice for now.
        output = subprocess.check_output(command)
        # Decode bytes output and remove surrounding whitespace, if any.
        # Usually there is a newline at the end, which we don't want.
        output = output.decode().strip()

        lines = output.split('\n')
        # Name of fortune file is on the first line surrounded by brackets.
        name = lines[0].strip('()')
        # The text starts on the third line.
        text = '\n'.join(lines[2:])

        return cls(text, fortune_file=name)

    def __str__(self):
        return self.text


Fortune = Cookie


def find_fortunes_path():
    """Find the installation path of fortunes on the system.

    :return: Directory containing fortune files.
    :rtype: :class:`pathlib.Path`
    :raise FileNotFoundError: No fortunes directory found on system.
    """
    for value in PATHS:
        if value.exists():
            return value

    raise FileNotFoundError('Could not find fortunes directory')


def find_fortune_files(path):
    """Find available fortune files inside of *path*.  That is: Any file that
    does not have '.u8' or '.dat' as suffix.  Running this on a path that does
    not (exclusively) contain actual fortune files is rather pointless.

    :param path: Directory containing fortune files.
    :type path: :class:`pathlib.Path` or :class:`str`
    :return: Iterator of fortune cookie database names.
    :rtype: generator of :class:`str`
    """
    # Convert path if it's a string.
    path = pathlib.Path(path)
    files = (x for x in path.iterdir() if x.is_file())
    names = (x for x in files if x.suffix not in ['.u8', '.dat'])
    return (x.name for x in names)
