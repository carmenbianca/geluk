============
Contributing
============

Contributions are always welcome.  This chapter will document how best to go
about contributing to the project.


Report bugs
-----------

The easiest way to contribute is to file reports about bugs you've run into
while using the program.  You can report bugs at
https://gitlab.com/carmenbianca/geluk/issues or mail the author if you're so
inclined.

Make sure that your bug report includes:

* Your operating system name and version.
* Any details about your setup that may be relevant in troubleshooting.
* Detailed steps to reproduce the bug.
* A detailed description of the bug itself.

Write code
----------

Writing code is the best and most fun way to contribute.  This assumes that
you're at least somewhat comfortable with Python and are running a distribution
of Linux.  openSUSE is assumed.  Those running Windows, OS X or another
distribution may have to translate some of these steps according to their
operating system.

First, fork the repository on GitLab and clone your fork::

    git clone git@gitlab.com:${YOUR_NAME}/geluk.git
    cd geluk/

Then set up your development environment::

    python3 -mvenv .venv  # or any other virtualenv name
    source .venv/bin/activate
    pip install -r requirements/dev.txt  # or just the requirements you need

PyQt5 is required to work on the GUI aspects of the project.  You can install
PyQt5 into your virtualenv in two ways.  The first works universally and will
link your system's installation of PyQt5 to your virtualenv::

    zypper install python3-qt5  # or install PyQt5 through any other means
    make link-pyqt

The second way requires Python 3.5 (or presumably higher) and a 64-bit
distribution.  Simply install PyQt5 from PyPI::

    pip install PyQt5

This should have your development environment covered.  To test, you can run
the project's test suite::

    # Run tests in current virtualenv.
    make test
    make lint
    make docs
    # or
    # Run all those tests with tox with multiple versions of Python.  Use
    # something like pyenv to have multiple local versions of Python.
    # Otherwise just ignore the test results from missing interpreters.
    make test-all

Now you can get coding.  Write whatever you like, add comments where necessary,
document what you've changed, write tests, add your changes to ``HISTORY.rst``,
add yourself to ``AUTHORS.rst``, make sure all tests succeed and submit a merge
request.  Easy!
