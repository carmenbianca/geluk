.DEFAULT_GOAL := help

define PRINT_HELP_PYSCRIPT
import re, sys

for line in sys.stdin:
	match = re.match(r'^([a-zA-Z_-]+):.*?## (.*)$$', line)
	if match:
		target, help = match.groups()
		print("%-20s %s" % (target, help))
endef
export PRINT_HELP_PYSCRIPT

.PHONY: help
help: ## show this text
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

.PHONY: clean
clean: clean-build clean-pyc clean-test clean-docs ## remove all build, test, coverage and Python artifacts

.PHONY: clean-build
clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -f geluk/_version.py
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

.PHONY: clean-pyc
clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

.PHONY: clean-tests
clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/

.PHONY: clean-docs
clean-docs: ## remove docs build artificats
	$(MAKE) -C docs clean
	rm -f docs/geluk.rst
	rm -f docs/modules.rst

.PHONY: lint
lint: ## check style with flake8
	flake8 geluk tests

.PHONY: test
test: clean-pyc ## run tests quickly with the default Python
	py.test

.PHONY: test-all
test-all: clean-pyc ## run tests on every Python version with tox
	tox

.PHONY: coverage
coverage: ## check code coverage quickly with the default Python
	py.test --cov-report term-missing --cov=geluk tests/

.PHONY: docs
docs: clean-docs ## generate Sphinx HTML documentation, including API docs
	sphinx-apidoc --no-toc --separate --module-first -o docs/ geluk
	$(MAKE) -C docs html

.PHONY: test-release
test-release: clean ## package and upload a release to test server
	NO_LOCAL_VERSION=true $(MAKE) dist
	twine upload -r pypitest -u $$PYPI_USER_NAME -p $$PYPI_PASSWORD dist/*

.PHONY: release
release: clean dist ## package and upload a release
	twine upload -r pypi -u $$PYPI_USER_NAME -p $$PYPI_PASSWORD dist/*

.PHONY: dist
dist: clean ## builds source and wheel package
	python setup.py sdist
	python setup.py bdist_wheel
	ls -l dist

.PHONY: install
install: clean ## install the package to the active Python's site-packages
	python setup.py install
	
.PHONY: develop
develop: clean ## install editable package
	python setup.py develop

.PHONY: link-pyqt
link-pyqt: ## link system PyQt5 into virtualenv
	/usr/bin/python3 scripts/link_pyqt.py ${VIRTUAL_ENV}
