#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright (C) 2016  Carmen Bianca Bakker <c.b.bakker@carmenbianca.eu>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest
import pathlib
import itertools
from unittest import mock
from unittest.mock import patch

from geluk import core


class TestCookie:

    def test_str(self):
        """__str__ magic method works."""
        text = 'Hello, world!'
        fortune = core.Cookie(text)

        assert str(fortune) == fortune.text == text


@patch('geluk.core.subprocess', autospec=True)
class TestGetCookie:

    FORTUNE_BYTES = b'(unexpected)\n%\nNobody expects\nthe' \
                    b' Spanish Inquisition\n'
    FORTUNE_TEXT = 'Nobody expects\nthe Spanish Inquisition'
    FORTUNE_FILE = 'unexpected'

    def test_base(self, mock_subprocess):
        """Calling without arguments correctly parses and returns a
        :class:`core.Cookie` object.
        """
        mock_subprocess.check_output.return_value = self.FORTUNE_BYTES

        result = core.Cookie.get_cookie()

        assert result.text == self.FORTUNE_TEXT
        assert result.fortune_file == self.FORTUNE_FILE
        mock_subprocess.check_output.assert_called_with(['fortune', '-c'])

    def test_fortune_files(self, mock_subprocess):
        """Providing 'fortune_files' keyword only returns cookies from those
        files.
        """
        mock_subprocess.check_output.return_value = self.FORTUNE_BYTES

        fortune_files = ['unexpected', 'unanticipated', 'unforeseen']

        core.Cookie.get_cookie(fortune_files=fortune_files)

        mock_subprocess.check_output.assert_called_with(['fortune', '-c']
                                                        + fortune_files)

    def test_offence_none(self, mock_subprocess):
        """Asking for no offensiveness doesn't add flags to command."""
        mock_subprocess.check_output.return_value = self.FORTUNE_BYTES

        core.Cookie.get_cookie(offence=core.Cookie.NOT_OFFENSIVE)

        mock_subprocess.check_output.assert_called_with(['fortune', '-c'])

    def test_offence_exclusive(self, mock_subprocess):
        """Asking for offensive cookies adds offensive flag to command."""
        mock_subprocess.check_output.return_value = self.FORTUNE_BYTES

        core.Cookie.get_cookie(offence=core.Cookie.OFFENSIVE)

        assert '-o' in itertools.chain(
            *mock_subprocess.check_output.call_args[0])

    def test_offence_all(self, mock_subprocess):
        """Asking for both offensive and non-offensive cookies adds flag to
        command.
        """
        mock_subprocess.check_output.return_value = self.FORTUNE_BYTES

        core.Cookie.get_cookie(offence=core.Cookie.ALL)

        assert '-a' in itertools.chain(
            *mock_subprocess.check_output.call_args[0])


class TestFindFortunesPath:

    MOCK_PATHS_LENGTH = len(core.PATHS)

    def create_paths(self, mock_paths):
        """Helper function to overwrite :data:`core.PATHS` with mocked paths.
        """
        paths = [mock.create_autospec(pathlib.Path) for
                 _ in range(self.MOCK_PATHS_LENGTH)]
        mock_paths.__iter__.return_value = paths

        # All paths do not exist.
        for path in paths:
            path.exists.return_value = False

        return paths

    @patch('geluk.core.PATHS', autospec=True)
    def test_path_none_found(self, mock_paths):
        """Raise :class:`FileNotFoundError` if no path exists."""
        self.create_paths(mock_paths)

        with pytest.raises(FileNotFoundError):
            core.find_fortunes_path()

    @patch('geluk.core.PATHS', autospec=True)
    def test_path_found(self, mock_paths):
        """Return path if one exists."""
        for i in range(self.MOCK_PATHS_LENGTH):
            paths = self.create_paths(mock_paths)
            paths[i].exists.return_value = True

            assert core.find_fortunes_path() == paths[i]


@patch('geluk.core.pathlib.Path.is_file', autospec=True)
@patch('geluk.core.pathlib.Path.iterdir', autospec=True)
class TestFindFortuneFiles:

    BASE_PATH = pathlib.Path('/usr/share/fortune')

    def test_general(self, mock_iterdir, mock_is_file):
        """Correctly identify fortune file."""
        directory = [
            self.BASE_PATH / 'zippy',
            self.BASE_PATH / 'zippy.u8',
            self.BASE_PATH / 'zippy.dat',
            self.BASE_PATH / 'off',
        ]

        mock_is_file.side_effect = [True, True, True, False]
        mock_iterdir.return_value = directory

        assert list(core.find_fortune_files(self.BASE_PATH)) == ['zippy']

        mock_iterdir.assert_called_with(self.BASE_PATH)

    def test_no_directories_allowed(self, mock_iterdir, mock_is_file):
        """Directories are not fortune files."""
        directory = [
            self.BASE_PATH / 'dir1',
            self.BASE_PATH / 'dir2',
        ]

        mock_is_file.return_value = False
        mock_iterdir.return_value = directory

        assert list(core.find_fortune_files(self.BASE_PATH)) == []

    def test_no_suffix_allowed(self, mock_iterdir, mock_is_file):
        """Files with suffixes are not the fortune files we want."""
        directory = [
            self.BASE_PATH / 'a.u8',
            self.BASE_PATH / 'a.dat',
        ]

        mock_is_file.return_value = True
        mock_iterdir.return_value = directory

        assert list(core.find_fortune_files(self.BASE_PATH)) == []

    def test_all_permitted(self, mock_iterdir, mock_is_file):
        """Files without suffixes are found."""
        directory = [
            self.BASE_PATH / 'a',
            self.BASE_PATH / 'b',
        ]

        mock_is_file.return_value = True
        mock_iterdir.return_value = directory

        assert list(core.find_fortune_files(self.BASE_PATH)) == ['a', 'b']

    def test_string_parameter(self, mock_iterdir, mock_is_file):
        """Function also accepts string parameter."""
        directory = [
            self.BASE_PATH / 'a',
            self.BASE_PATH / 'b',
        ]

        mock_is_file.return_value = True
        mock_iterdir.return_value = directory

        assert list(core.find_fortune_files(str(self.BASE_PATH))) == ['a', 'b']
