=====================
Geluk's documentation
=====================

Contents:

.. toctree::
   :maxdepth: 2

   readme
   installation
   usage
   contributing
   authors
   history

=======
Modules
=======

.. toctree::
   :maxdepth: 3

   geluk

=====================
Indices and searching
=====================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
